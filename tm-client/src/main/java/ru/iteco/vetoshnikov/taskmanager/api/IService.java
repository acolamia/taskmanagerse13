package ru.iteco.vetoshnikov.taskmanager.api;

import java.util.Scanner;

public interface IService {
    Scanner getScanner();
}
