package ru.iteco.vetoshnikov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.Session;
import ru.iteco.vetoshnikov.taskmanager.command.AbstractCommand;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "удаляет выбранный вами проект.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSessionStatusService().getSession();
        System.out.print("Введите имя проекта, который хотите удалить: ");
        @Nullable final String projectName = service.getScanner().nextLine();
        serviceLocator.getProjectEndpointService().getProjectEndpointPort().removeProject(session.getUserId(), projectName);
    }
}
