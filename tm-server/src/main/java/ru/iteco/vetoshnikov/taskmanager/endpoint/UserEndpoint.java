package ru.iteco.vetoshnikov.taskmanager.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.vetoshnikov.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createUserUser(
            @WebParam(name = "userObject") @Nullable final User userObject
    ) {
        serviceLocator.getUserService().createUser(userObject);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "userObject") @Nullable final User userObject
    ) {
        serviceLocator.getUserService().merge(userObject);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "userId") @Nullable final String userId
    ) {
        serviceLocator.getUserService().remove(userId);
    }

    @Override
    @WebMethod
    public void clearUser(
    ) {
        serviceLocator.getUserService().clear();
    }

    @Override
    @WebMethod
    public User findOneUser(
            @WebParam(name = "userName") @Nullable final String userName
    ) {
        return serviceLocator.getUserService().findOne(userName);
    }

    @Override
    @WebMethod
    public List<User> findAllUser(
    ) {
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "domainObject") @Nullable final Domain domainObject
    ) {
        serviceLocator.getUserService().load(domainObject);
    }

    @Override
    @WebMethod
    public String getIdUserUser(
            @WebParam(name = "userName") @Nullable final String userName
    ) {
        return serviceLocator.getUserService().getIdUser(userName);
    }
}
