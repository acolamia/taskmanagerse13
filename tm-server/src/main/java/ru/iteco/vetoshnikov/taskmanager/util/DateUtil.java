package ru.iteco.vetoshnikov.taskmanager.util;

import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    @Nullable
    public static Date parseDate(@Nullable final String string) {
        if (string == null || string.isEmpty()) {
            return null;
        }
        try {
             SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd / yyyy hh: mm:ss a");
            return dateFormat.parse(string);
        } catch (Exception e) {
            return null;
        }
    }
}
