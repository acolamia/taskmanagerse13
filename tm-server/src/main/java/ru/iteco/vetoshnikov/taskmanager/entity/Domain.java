package ru.iteco.vetoshnikov.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "domain")
@JacksonXmlRootElement(localName = "domain")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain extends AbstractEntity {
    @XmlElementWrapper(name = "users")
    @XmlElement(name = "user")
    private List<User> userList = new ArrayList<>();
    @XmlElementWrapper(name = "projects")
    @XmlElement(name = "project")
    private List<Project> projectList = new ArrayList<>();
    @XmlElementWrapper(name = "tasks")
    @XmlElement(name = "task")
    private List<Task> taskList = new ArrayList<>();
}
