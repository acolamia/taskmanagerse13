package ru.iteco.vetoshnikov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IProjectService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import lombok.*;
import ru.iteco.vetoshnikov.taskmanager.util.SqlUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {
    SqlSessionFactory sqlSessionFactory;
    SqlSession sqlSession;

    @Override
    public void createProject(@Nullable final Project project) {
        if (project == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectRepository.persist(project);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectRepository.merge(project);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final List<Project> projectList) {
        if (projectList == null || projectList.isEmpty()) return;
        for (@Nullable final Project project : projectList) {
            if (project == null) continue;
            try {
                sqlSessionFactory = SqlUtil.getSqlSessionFactory();
                sqlSession = sqlSessionFactory.openSession();
                ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
                projectRepository.merge(project);
                sqlSession.commit();
            } catch (Exception e) {
                sqlSession.rollback();
                e.printStackTrace();
            } finally {
                sqlSession.close();
            }
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectRepository.remove(userId, projectName);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAllByUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectRepository.removeAllByUser(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear() {
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectRepository.clear();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty() || projectName == null || projectName.isEmpty()) return null;
        Project project = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            project = projectRepository.findOne(userId, projectName);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    public @Nullable
    final List<Project> findAll() {
        List<Project> projectList = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectList = projectRepository.findAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return projectList;
    }

    @Override
    public @Nullable
    final List<Project> findAllByUser(@Nullable final String userId) {
        @Nullable List<Project> projectList = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectList = projectRepository.findAllByUser(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return projectList;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            projectRepository.clear();
            projectRepository.load(domain);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public String getIdProject(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty() || name == null || name.isEmpty()) return null;
        String projectId = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            Project project=projectRepository.getIdProject(userId, name);
            projectId = project.getId();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return projectId;
    }
}
