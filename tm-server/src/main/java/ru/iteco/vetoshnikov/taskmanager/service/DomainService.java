package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IDomainService;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;

@Getter
@Setter
@RequiredArgsConstructor
public final class DomainService implements IDomainService {
    private final IServiceLocator serviceLocator;

    @Override
    public void save(@Nullable Domain domain) {
        domain.setUserList(serviceLocator.getUserService().findAll());
        domain.setProjectList(serviceLocator.getProjectService().findAll());
        domain.setTaskList(serviceLocator.getTaskService().findAll());
    }

    @Override
    public void load(@Nullable Domain domain)  {
        if (domain == null) return;
        serviceLocator.getUserService().load(domain);
        serviceLocator.getProjectService().load(domain);
        serviceLocator.getTaskService().load(domain);
    }
}

