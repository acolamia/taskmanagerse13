package ru.iteco.vetoshnikov.taskmanager.repository;

import org.apache.ibatis.annotations.Insert;
import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

public interface SessionRepository {

    @Insert("INSERT INTO app_session " +
            "(id, timestamp, user_id, signature)" +
            " VALUES (#{id},#{timestamp},#{userId},#{signature})")
    void createSession(@NotNull final Session session);
}
