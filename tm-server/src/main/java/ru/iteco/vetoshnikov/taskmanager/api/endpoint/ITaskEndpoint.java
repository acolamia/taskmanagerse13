package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void mergeTask(
            @WebParam(name = "taskObject") @Nullable Task taskObject
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );

    @WebMethod
    void clearTask(
    );

    @WebMethod
    Task findOneTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );

    @WebMethod
    List<Task> findAllTask(
    );

    @WebMethod
    List<Task> findAllByProjectTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void createTaskTask(
            @WebParam(name = "taskObject") @Nullable Task taskObject
    );

    @WebMethod
    void removeAllByProjectTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId
    );

    @WebMethod
    void loadTask(
            @WebParam(name = "domainObject") @Nullable Domain domainObject
    );

    @WebMethod
    String getIdTaskTask(
            @WebParam(name = "userId") @Nullable String  userId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "taskName") @Nullable String taskName
    );
}
