package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {
    @WebMethod
    void createUserUser(
            @WebParam(name = "userObject") @Nullable User userObject
    );

    @WebMethod
    void mergeUser(
            @WebParam(name = "userObject") @Nullable User userObject
    );

    @WebMethod
    void removeUser(
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void clearUser(
    );

    @WebMethod
    User findOneUser(
            @WebParam(name = "userName") @Nullable String userName
    );

    @WebMethod
    List<User> findAllUser(
    ) ;

    @WebMethod
    void loadUser(
            @WebParam(name = "domainObject") @Nullable Domain domainObject
    );

    @WebMethod
    String getIdUserUser(
            @WebParam(name = "userName") @Nullable String userName
    );
}
