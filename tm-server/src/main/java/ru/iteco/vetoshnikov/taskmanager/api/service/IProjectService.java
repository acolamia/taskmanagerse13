package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

import java.util.List;

public interface IProjectService {
    void createProject(@Nullable Project project);

    void merge(@Nullable Project project);

    void merge(@Nullable List<Project> projectList);

    void remove(@Nullable String userId, String projectName);

    void removeAllByUser(@Nullable String userId);

    void clear();

    Project findOne(@Nullable String userId, String projectName);

    @Nullable List<Project> findAll();

    @Nullable List<Project> findAllByUser(String userId);

    void load(@Nullable Domain domain);

    String getIdProject(@Nullable String userId, @Nullable String name);
}
