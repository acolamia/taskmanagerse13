package ru.iteco.vetoshnikov.taskmanager.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.repository.ProjectRepository;
import ru.iteco.vetoshnikov.taskmanager.repository.SessionRepository;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.Properties;

public final class SqlUtil {
    public static SqlSessionFactory getSqlSessionFactory() throws Exception {

        final InputStream fis = SqlUtil.class.getClassLoader().getResourceAsStream("mysql.properties");
        final Properties property = new Properties();
        property.load(fis);

        @Nullable final String driver = property.getProperty("db.driver");
        @Nullable final String url = property.getProperty("db.host");
        @Nullable final String user = property.getProperty("db.login");
        @Nullable final String password = property.getProperty("db.password");

        final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(UserRepository.class);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(TaskRepository.class);
        configuration.addMapper(SessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
