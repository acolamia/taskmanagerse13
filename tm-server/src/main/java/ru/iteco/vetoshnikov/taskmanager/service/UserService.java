package ru.iteco.vetoshnikov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.IUserService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.UserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.SqlUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class UserService extends AbstractService implements IUserService {
    SqlSessionFactory sqlSessionFactory;
    SqlSession sqlSession;

    @Override
    public void createUser(@Nullable final User user) {
        if (user == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            String getUser = userRepository.checkUser(user.getLogin());
            if (getUser == null||getUser.isEmpty()){
                userRepository.persist(user);
            }
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            userRepository.merge(user);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<User> merge(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) return null;
        for (@Nullable final User user : userList) {
            if (user == null) continue;
            try {
                sqlSessionFactory = SqlUtil.getSqlSessionFactory();
                sqlSession = sqlSessionFactory.openSession();
                UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
                userRepository.merge(user);
                sqlSession.commit();
            } catch (Exception e) {
                sqlSession.rollback();
                e.printStackTrace();
            } finally {
                sqlSession.close();
            }
        }
        return userList;
    }

    @Override
    public void remove(@Nullable final String key) {
        if (key == null || key.isEmpty()) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            userRepository.remove(key);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public User findOne(@Nullable final String key) {
        if (key == null || key.isEmpty()) return null;
        User user = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            user = userRepository.findOne(key);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public @Nullable
    final List<User> findAll() {
        List<User> userList = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            userList = userRepository.findAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return userList;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            userRepository.clear();
            userRepository.load(domain);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public String getIdUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        String userId = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            userId = userRepository.getIdUser(login);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return userId;
    }

    @Override
    public void clear() {
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }
}
