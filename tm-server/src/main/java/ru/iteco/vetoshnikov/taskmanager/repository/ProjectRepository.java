package ru.iteco.vetoshnikov.taskmanager.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public interface ProjectRepository {

    @Update("UPDATE app_project SET " +
            "(user_id=#{userId}, " +
            "statusType=#{statusType}, " +
            "name=#{name}, " +
            "description=#{description}, " +
            "beginDate=#{beginDate}, " +
            "endDate=#{endDate}, " +
            "createDate=#{createDate}) " +
            " WHERE id=#{id}")
    void merge(@NotNull final Project project);

    @Delete("DELETE FROM app_project WHERE (user_id=#{arg0} AND name=#{arg1})")
    void remove(@NotNull final String userId, @NotNull final String projectName);

    @Delete("DELETE FROM app_project")
    void clear();

    @Select("SELECT * FROM app_project WHERE user_id  AND name VALUES (#{arg0},#{arg1})")
    Project findOne(@Nullable final String userId, @Nullable final String projectName);

    @Select("SELECT * FROM app_project")
    List<Project> findAll();

    @Select("SELECT * FROM app_project WHERE (user_id=#{arg0})")
    List<Project> findAllByUser(@NotNull final String userId);

    @Insert("INSERT INTO app_project (id, user_id, name, statusType, description, beginDate, endDate, createDate) " +
            " VALUES (#{id},#{userId},#{name},#{statusType},#{description},#{beginDate},#{endDate},#{createDate})")
    void persist(@NotNull final Project project);

    @Delete("DELETE FROM app_project WHERE (user_id=#{arg0})")
    void removeAllByUser(@NotNull final String userId);

    @Insert("INSERT INTO app_project " +
            "(id=#{id}, " +
            "user_id=#{userId}, " +
            "statusType=#{statusType}, " +
            "name=#{name}, " +
            "description=#{description}, " +
            "beginDate=#{beginDate}, " +
            "endDate=#{endDate}, " +
            "createDate=#{createDate})")
    void load(@NotNull final Domain domain);

    @Select("Select * from app_project where user_id = #{arg0} AND name = #{arg1}")
    Project getIdProject(@NotNull final String userId, @NotNull final String name);
}
