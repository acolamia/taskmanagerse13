package ru.iteco.vetoshnikov.taskmanager.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public interface TaskRepository {
    @Update("UPDATE app_task SET " +
            "(project_id=#{projectId}," +
            "user_id=#{userId}," +
            "statusType=#{statusType}" +
            ",name=#{name}," +
            "description=#{description}" +
            ",beginDate=#{beginDate}," +
            "endDate=#{endDate}," +
            "createDate=#{createDate}) " +
            " WHERE (id=#{id})")
    void merge(@NotNull final Task task);

    @Delete("DELETE FROM app_task WHERE (user_id=#{arg0} AND project_id=#{arg1} AND name=#{arg2})")
    void remove(@Nullable final String userId, String projectId, String taskName);

    @Delete("DELETE FROM app_task")
    void clear();

    @Select("SELECT * FROM app_task WHERE (user_id=#{arg0} AND project_id=#{arg1} AND name=#{arg2})")
    Task findOne(@NotNull final String userId, String projectId, String taskName);

    @Select("SELECT * FROM app_task")
    List<Task> findAll();

    @Select("SELECT * FROM app_task WHERE (user_id=#{arg0} AND project_id=#{arg1})")
    List<Task> findAllByProject(@NotNull final String userId, @NotNull final String projectId);

    @Insert("INSERT INTO app_task (id, project_id, user_id, name, statusType, description, beginDate, endDate, createDate) "+
            " VALUES (#{id},#{projectId},#{userId},#{name},#{statusType},#{description},#{beginDate},#{endDate},#{createDate})")
    void persist(@NotNull final Task task);

    @Delete("DELETE FROM app_task WHERE (user_id=#{arg0} AND project_id=#{arg1})")
    void removeAllByProject(@Nullable final String userId, String projectId);

    @Insert("INSERT INTO app_task " +
            "(id, project_id, user_id,name, statusType,  description, beginDate, endDate, createDate) "+
    " VALUES (#{id},#{projectId},#{userId},#{name},#{statusType},#{description},#{beginDate},#{endDate},#{createDate})")
    void load(@NotNull final Domain domain);

    @Select("SELECT * FROM app_project WHERE " +
            "(project_id=#{arg0} AND " +
            "user_id=#{arg1} AND " +
            "name=#{arg2})")
    String getIdTask(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name);
}
