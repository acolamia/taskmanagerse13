package ru.iteco.vetoshnikov.taskmanager.constant;

public final class FieldConst {
    public static final String ID = "id";
    public static final String USER_ID = "user_Id";
    public static final String PROJECT_ID = "project_Id";
    public static final String CREATE_DATE = "createDate";
    public static final String BEGIN_DATE = "beginDate";
    public static final String END_DATE = "endDate";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ROLE_TYPE = "role";
    public static final String STATUS_TYPE = "statusType";
    public static final String TIMESTAMP = "timestamp";
    public static final String SIGNATURE = "signature";
}
