package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    void mergeProject(
            @WebParam(name = "projectObject") @Nullable Project projectObject
    );

    @WebMethod
    void removeProject(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );

    @WebMethod
    void clearProject();

    @WebMethod
    Project findOneProject(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );

    @WebMethod
    List<Project> findAllProject();

    @WebMethod
    List<Project> findAllByUserProject(
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void createProjectProject(
            @WebParam(name = "projectObject") @Nullable Project projectObject
    );

    @WebMethod
    void removeAllByUserProject(
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void loadProject(
            @WebParam(name = "domainObject") @Nullable Domain domainObject
    );

    @WebMethod
    String getIdProjectProject(
            @WebParam(name = "userId") @Nullable String userId,
            @WebParam(name = "projectName") @Nullable String projectName
    );
}