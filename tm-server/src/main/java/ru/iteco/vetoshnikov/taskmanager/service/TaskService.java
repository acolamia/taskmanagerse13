package ru.iteco.vetoshnikov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.service.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.Task;
import ru.iteco.vetoshnikov.taskmanager.repository.TaskRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.iteco.vetoshnikov.taskmanager.util.SqlUtil;

import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {
    SqlSessionFactory sqlSessionFactory;
    SqlSession sqlSession;

    @Override
    public void createTask(@Nullable final Task task) {
        if (task == null || task.getProjectId() == null || task.getProjectId().isEmpty()) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskRepository.persist(task);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskRepository.merge(task);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Task> merge(@Nullable final List<Task> taskList) {
        if (taskList == null || taskList.isEmpty()) return null;
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            try {
                sqlSessionFactory = SqlUtil.getSqlSessionFactory();
                sqlSession = sqlSessionFactory.openSession();
                TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
                taskRepository.merge(task);
                sqlSession.commit();
            } catch (Exception e) {
                sqlSession.rollback();
                e.printStackTrace();
            } finally {
                sqlSession.close();
            }
        }
        return taskList;
    }

    @Override
    public void remove(@Nullable final String userId, String projectId, String taskName) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskRepository.remove(userId, projectId, taskName);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAllByProject(@Nullable final String userId, String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskRepository.removeAllByProject(userId, projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear() {
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskRepository.clear();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Task findOne(@Nullable final String userId, String projectId, String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        Task task = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            task = taskRepository.findOne(userId, projectId, taskName);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    public @Nullable
    final List<Task> findAll() {
        List<Task> taskList = null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskList = taskRepository.findAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return taskList;
    }

    @Override
    public @Nullable
    final List<Task> findAllByProject(String userId, String projectId) {
        List<Task>taskList=null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskList=taskRepository.findAllByProject(userId,projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return taskList;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskRepository.clear();
            taskRepository.load(domain);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public String getIdTask(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        String taskId=null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            taskId=taskRepository.getIdTask(userId,projectId,taskName);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return taskId;
    }
}