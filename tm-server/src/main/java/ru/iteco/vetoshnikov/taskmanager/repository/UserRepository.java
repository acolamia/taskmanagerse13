package ru.iteco.vetoshnikov.taskmanager.repository;

import org.apache.ibatis.annotations.*;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface UserRepository {

    @Update("UPDATE app_user SET " +
            "(login=#{login}," +
            "password=#{password}," +
            "role=#{role}," +
            "name=#{name}," +
            "description=#{description}," +
            "beginDate=#{beginDate}," +
            "endDate=#{endDate}," +
            "createDate=#{createDate}) " +
            " WHERE (id=#{id})")
    void merge(@NotNull final User user);

    @Delete("DELETE FROM app_user WHERE (id=#{arg0})")
    void remove(@NotNull final String key);

    @Delete("DELETE FROM app_user")
    void clear();

    @Select("SELECT * FROM app_user WHERE (id=#{arg0}) limit 1")
    User findOne(@NotNull final String key);

    @Select("SELECT * FROM app_user")
    List<User> findAll();

    @Insert("INSERT INTO app_user (id, login, password, role, name, description, beginDate, endDate, createDate)" +
            " VALUES (#{id},#{login},#{password},#{role},#{name},#{description},#{beginDate},#{endDate},#{createDate})")
    void persist(@NotNull final User user);

    @Insert("INSERT INTO app_user " +
            "(id=#{id}," +
            "login=#{login}," +
            "password=#{password}," +
            "role=#{role}," +
            "name=#{name}," +
            "description=#{description}," +
            "beginDate=#{beginDate}," +
            "endDate=#{endDate}," +
            "createDate=#{createDate})")
    void load(@NotNull final Domain domain);

    @Select("SELECT * FROM app_user WHERE (login = #{arg0})")
    String getIdUser(@NotNull final String login);

    @Select("SELECT name FROM app_user WHERE (login=#{arg0}) limit 1")
    String checkUser(@NotNull final String login);
}
