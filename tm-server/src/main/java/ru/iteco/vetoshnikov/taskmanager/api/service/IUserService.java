package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;
import ru.iteco.vetoshnikov.taskmanager.entity.User;

import java.util.List;

public interface IUserService {
    void createUser(@Nullable User user);

    void merge(@Nullable User user);

    List<User> merge(@Nullable List<User> userList);

    void remove(@Nullable String key);

    User findOne(@Nullable String key);

    @Nullable List<User> findAll();

    void load(@Nullable Domain domain);

    String getIdUser(@Nullable String login);

    void clear();
}
