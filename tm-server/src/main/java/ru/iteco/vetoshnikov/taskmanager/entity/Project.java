package ru.iteco.vetoshnikov.taskmanager.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.enumerate.StatusType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity  {
    @NotNull
    private String statusType = StatusType.PLANNED.getDisplayName();
    @Nullable
    private String userId = null;
}
