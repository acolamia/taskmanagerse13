package ru.iteco.vetoshnikov.taskmanager.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.api.IServiceLocator;
import ru.iteco.vetoshnikov.taskmanager.api.service.ISessionService;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;
import ru.iteco.vetoshnikov.taskmanager.entity.User;
import ru.iteco.vetoshnikov.taskmanager.repository.SessionRepository;
import ru.iteco.vetoshnikov.taskmanager.util.HashUtil;
import ru.iteco.vetoshnikov.taskmanager.util.SignatureUtil;
import ru.iteco.vetoshnikov.taskmanager.util.SqlUtil;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {
    private IServiceLocator serviceLocator;
    SqlSessionFactory sqlSessionFactory;
    SqlSession sqlSession;

    public SessionService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public User checkPassword(
            @Nullable final String userLogin,
            @Nullable final String userPassword
    ) {
        if (userLogin == null || userLogin.isEmpty()) return null;
        if (userPassword == null || userPassword.isEmpty()) return null;
        @Nullable final List<User> userList = serviceLocator.getUserService().findAll();
        for (User getUser : userList) {
            if (userLogin.equals(getUser.getLogin())) {
                @NotNull final User user = getUser;
                @Nullable final String passwordHash = HashUtil.getHash(userPassword);
                if (passwordHash == null) return null;
                if (passwordHash.equals(user.getPassword()))
                    return user;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Session createSession(
            @Nullable final String userId
    ) {
        Session session = new Session();
        if (userId == null || userId.isEmpty()) return null;
        try {
            sqlSessionFactory = SqlUtil.getSqlSessionFactory();
            sqlSession = sqlSessionFactory.openSession();
            SessionRepository sessionRepository = sqlSession.getMapper(SessionRepository.class);
            session.setUserId(userId);
            @Nullable final String signature = SignatureUtil.sign(session);
            if (signature == null) return null;
            session.setSignature(signature);
            sessionRepository.createSession(session);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return session;
    }
}
