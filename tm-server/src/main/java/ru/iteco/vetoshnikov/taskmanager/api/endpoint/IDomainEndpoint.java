package ru.iteco.vetoshnikov.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.iteco.vetoshnikov.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {
    @WebMethod
    void loadBinary(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void saveBinary(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void loadFasterJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void saveFasterJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void loadFasterXml(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void saveFasterXml(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void loadJaxbJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void saveJaxbJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void loadJaxbXml(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    void saveJaxbXml(@WebParam(name = "session") @Nullable Session session);
}
