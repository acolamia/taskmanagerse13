package ru.iteco.vetoshnikov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.iteco.vetoshnikov.taskmanager.entity.Domain;

public interface IDomainService {
    void load(@NotNull final Domain domain);

    void save(@NotNull final Domain domain);
}
